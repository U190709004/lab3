public class FindPrimes
{

    public static void main(String[] args)
    {
        String  primeNumbers = "";

        int x = Integer.parseInt(args[0]);
        int test=0;
        int num=0;


        for (test=1; test<=x; test++)
        {
        int xd=0;
            for (num=test; num>=1; num--)
            {
                if (test % num == 0)
                {
                    xd++;
                }
            }
                if (xd == 2)
                {
                    primeNumbers = primeNumbers + test + ",";
                }
        }

        System.out.println("Prime numbers from 1 to given number");
        System.out.println(primeNumbers);
    }

}
